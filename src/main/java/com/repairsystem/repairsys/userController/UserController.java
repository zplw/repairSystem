package com.repairsystem.repairsys.userController;

import com.repairsystem.repairsys.common.Const;
import com.repairsystem.repairsys.common.ResponseCode;
import com.repairsystem.repairsys.common.ServerResponse;
import com.repairsystem.repairsys.pojo.User;
import com.repairsystem.repairsys.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user/")
@CrossOrigin
public class UserController {

    @Autowired
    private IUserService iUserService;


    /**
     * 用户登录
     * @param userNum   用户账号
     * @param userPassword  用户密码
     * @param session
     * @return
     */
    @RequestMapping(value = "login.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> login (String userNum, String userPassword, HttpSession session){
        ServerResponse<User> response = iUserService.login(userNum, userPassword);
        if (response.isSuccess()){
            session.setAttribute(Const.CURRENT_USER,response.getData());
        }
        return response;
    }

    /**
     * 用户登出
     * @param session
     * @return
     */
    @RequestMapping(value = "logout.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> logout(HttpSession session){
        session.removeAttribute(Const.CURRENT_USER);
        return ServerResponse.createBySuccess();
    }

    /**
     * 获取用户登录信息
     * @param session
     * @return
     */
    @RequestMapping(value = "get_user_info.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> getUserInfo(HttpSession session){
        User user = (User)session.getAttribute(Const.CURRENT_USER);
        if(user != null){
            return ServerResponse.createBySuccess(user);
        }
        return ServerResponse.createByErrorMassage("用户未登录，无法获取当前用户信息");
    }

    /**
     * 分页,查看故障报修订单
     * @param pageNum
     * @param pageSize
     * @param session
     * @return
     */
    @RequestMapping(value = "fault_list.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse faultList(@RequestParam(value = "pageNum",required = false,defaultValue = "1")Integer pageNum,
                               @RequestParam(value = "pageSize",required = false,defaultValue = "5")Integer pageSize,
                               HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        return iUserService.list(pageNum,pageSize);
    }
    /**
     * 分页,按故障类型进行检索
     * @param pageNum
     * @param pageSize
     * @param session
     * @param faultCategoryId
     * @return
     */
    @RequestMapping(value = "select_fault_by_category.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse selectFaultByCategory(@RequestParam(value = "pageNum",required = false,defaultValue = "1")Integer pageNum,
                               @RequestParam(value = "pageSize",required = false,defaultValue = "5")Integer pageSize,
                               String faultCategoryId,HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        return iUserService.selectFaultByCategory(pageNum,pageSize,faultCategoryId);
    }

    /**
     * 检索所有故障类型
     * @param session
     * @return
     */
    @RequestMapping(value = "select_all_fault_category.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse selectAllFaultCategory(HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        return iUserService.selectAllFaultCategory();
    }

    /**
     * 分页,检索用户的故障订单
     * @param pageNum
     * @param pageSize
     * @param session
     * @param userId
     * @return
     */
    @RequestMapping(value = "select_fault_by_user_id.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse selectFaultByUserId(@RequestParam(value = "pageNum",required = false,defaultValue = "1")Integer pageNum,
                                                @RequestParam(value = "pageSize",required = false,defaultValue = "5")Integer pageSize,
                                                String userId,HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        return iUserService.selectFaultByUserId(pageNum,pageSize,userId);
    }

    /**
     * 更改密码
     * @param session
     * @param oldPassword
     * @param newPassword
     * @param newPasswordAgain
     * @return
     */
    @RequestMapping(value = "new_password.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse newPassword(HttpSession session,String userId,String oldPassword,String newPassword,String newPasswordAgain) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        return iUserService.newPassword(userId,oldPassword,newPassword,newPasswordAgain);
    }

    @RequestMapping(value = "user_comment.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse userComment(HttpSession session,String commentText,String faultId,String maintainerId) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        return iUserService.userComment(commentText,faultId,maintainerId);
    }

//    @RequestMapping(value = "submit_error_info",method = RequestMethod.GET)
//    @ResponseBody
//    public ServerResponse submitErrorInfo(Integer faultCategoryId,String faultDescribe,
//                                          String faultSite,HttpSession session){
//        User user = (User)session.getAttribute(Const.CURRENT_USER);
//        if(user == null){
//            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
//        }
//        Fault fault = new Fault();
//        fault.setFaultCategoryId(faultCategoryId);
//        fault.setFaultDescribe(faultDescribe);
//        fault.setFaultSite(faultSite);
//        fault.setFaultTime(new Date());
//
//        return null;
//    }
}
