package com.repairsystem.repairsys.service;

import com.github.pagehelper.PageInfo;
import com.repairsystem.repairsys.common.ServerResponse;
import com.repairsystem.repairsys.pojo.Admin;
import com.repairsystem.repairsys.pojo.FaultCategory;

import java.util.List;

public interface IAdminService {
    ServerResponse<Admin> login(String adminNum, String adminPassword);

    ServerResponse<String> checkValid(String str,String type);

    ServerResponse<String> addTentativeFaultOder(String faultId);

    ServerResponse<String> addFaultCategory(FaultCategory faultCategory);

    ServerResponse<String> deleteFaultCategory(String faultCategoryId);

    ServerResponse<List> selectAllAdmin();

    ServerResponse<PageInfo> selectAllUserOrder(Integer pageNum, Integer pageSize);

    ServerResponse<PageInfo> selectAllFaultCategoryByPage(Integer pageNum, Integer pageSize);

    ServerResponse newPassword(String adminId, String oldPassword, String newPassword, String newPasswordAgain);

    ServerResponse deleteFault(String faultId);
}
