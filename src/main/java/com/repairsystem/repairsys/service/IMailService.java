package com.repairsystem.repairsys.service;

import com.repairsystem.repairsys.common.ServerResponse;

public interface IMailService {

    ServerResponse sendEmail(String userId,String cause);
}
