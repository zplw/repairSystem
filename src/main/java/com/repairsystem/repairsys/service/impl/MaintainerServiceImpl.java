package com.repairsystem.repairsys.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.repairsystem.repairsys.common.Const;
import com.repairsystem.repairsys.common.ResponseCode;
import com.repairsystem.repairsys.common.ServerResponse;
import com.repairsystem.repairsys.dao.CommentMapper;
import com.repairsystem.repairsys.dao.FaultCategoryMapper;
import com.repairsystem.repairsys.dao.FaultMapper;
import com.repairsystem.repairsys.dao.MaintainerMapper;
import com.repairsystem.repairsys.pojo.Comment;
import com.repairsystem.repairsys.pojo.Fault;
import com.repairsystem.repairsys.pojo.FaultCategory;
import com.repairsystem.repairsys.pojo.Maintainer;
import com.repairsystem.repairsys.service.IMaintainerService;
import com.repairsystem.repairsys.vo.CommentVo;
import com.repairsystem.repairsys.vo.RepairFaultVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service("iMaintainerService")
@Transactional
public class MaintainerServiceImpl implements IMaintainerService {
    @Autowired
    private MaintainerMapper maintainerMapper;
    @Autowired
    private FaultMapper faultMapper;
    @Autowired
    private FaultCategoryMapper faultCategoryMapper;
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public ServerResponse<Maintainer> login(String maintainerNum, String maintainerPassword) {
        ServerResponse<String> validResponse = this.checkVaild(maintainerNum, Const.NUM);
        if(validResponse.isSuccess()) {
            return ServerResponse.createByErrorMassage("用户名不存在！！！");
        }
        Maintainer Maintainer = maintainerMapper.selectLogin(maintainerNum, maintainerPassword);
        if(Maintainer == null){
            return ServerResponse.createByErrorMassage("密码错误！！！");
        }
        Maintainer.setMaintainerPassword(StringUtils.EMPTY);
        return ServerResponse.createBySuccess("登录成功！",Maintainer);
    }

    public ServerResponse<String> checkVaild(String str,String type){
        if(StringUtils.isNotBlank(type)){
            if(Const.NUM.equals(type)){
               int resultCount =  maintainerMapper.checkMaintainnum(str);
               if(resultCount > 0){
                   return ServerResponse.createByErrorMassage("用户名已存在");
               }
            }
            if(Const.TEL.equals(type)){
                int resultCount = maintainerMapper.checkTel(str);
                if(resultCount > 0){
                    return ServerResponse.createByErrorMassage("电话已存在");
                }
            }
        }else{
            return ServerResponse.createByErrorMassage("参数错误");
        }
        return ServerResponse.createBySuccessMassage("校验成功");
        }

    @Override
    public ServerResponse<List> selectAllMaintain() {

        List<Maintainer> maintainerList = maintainerMapper.selectAllMaintain();

        if(maintainerList.size() == 0 || maintainerList == null){
            return ServerResponse.createByErrorMassage("没有任何员工信息");
        }else{
            return ServerResponse.createBySuccess(maintainerList);
        }
    }

    @Override
    public ServerResponse<PageInfo> selectAllAdminOrder(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Fault> faultList = faultMapper.selectAllAdminOrder();
        List<FaultCategory> faultCategoryList = faultCategoryMapper.selectAllFaultCategory();
        List<RepairFaultVo> repairFaultVoList = Lists.newArrayList();
        if(faultList == null || faultList.size() == 0){
            return ServerResponse.createByErrorMassage("暂时还没有可接的订单！");
        }
        for (Fault fault:faultList) {
            RepairFaultVo repairFaultVo = createRepairFaultVo(fault,faultCategoryList);
            repairFaultVoList.add(repairFaultVo);
        }
        PageInfo pageInfo = new PageInfo(faultList);
        pageInfo.setList(repairFaultVoList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse takeOrder(String faultId, String maintainerId) {
        if(faultId == null || maintainerId == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),"参数错误");
        }
        int resultCount1 = faultMapper.selectMaintainerTakeOrderById(maintainerId);
        if(resultCount1 > 0){
            return ServerResponse.createByErrorMassage("存在未完成的订单，请完成后再接单");
        }
        int resultCount2 = faultMapper.checkFaultStatus(faultId,"2");
        if(resultCount2 > 0){
            return ServerResponse.createByErrorMassage("该订单已被其他维修员接单，请勿重复接单");
        }
        Fault fault = faultMapper.selectByPrimaryKey(Long.parseLong(faultId));
        if(fault == null || fault.getFaultStatus() != 1){
            return ServerResponse.createByErrorMassage("订单号异常");
        }
        fault.setFaultMaintainerId(Long.parseLong(maintainerId));
        byte status = 2;
        fault.setFaultStatus(status);
        int resultCount = faultMapper.updateByPrimaryKeySelective(fault);
        if(resultCount > 0){
            return ServerResponse.createBySuccess();
        }
        return ServerResponse.createByError();
    }

    @Override
    public ServerResponse selectMaintainerOrder(String maintainerId) {
        if(maintainerId == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),"参数错误");
        }
        Fault fault = faultMapper.selectMaintainerOrder(maintainerId);
        if(fault != null){
            List<FaultCategory> faultCategoryList = faultCategoryMapper.selectAllFaultCategory();
            RepairFaultVo repairFaultVo = createRepairFaultVo(fault,faultCategoryList);
            return ServerResponse.createBySuccess(repairFaultVo);
        }
        return ServerResponse.createByErrorMassage("您还没有接单哦！");
    }

    @Override
    public ServerResponse successOrder(String faultId, String faultSolution) {
        if(faultId == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),"参数错误");
        }
        Fault fault = faultMapper.selectByPrimaryKey(Long.parseLong(faultId));
        if(fault == null || fault.getFaultStatus() != 2){
            return ServerResponse.createByErrorMassage("订单号异常");
        }
        byte status = 3;
        fault.setFaultStatus(status);
        fault.setFaultSolutionTime(new Date());
        fault.setFaultSolution(faultSolution);
        int resultCount = faultMapper.updateByPrimaryKeySelective(fault);
        if(resultCount > 0){
            return ServerResponse.createBySuccess();
        }
        return ServerResponse.createByError();
    }

    @Override
    public ServerResponse newPassword(String maintainerId, String oldPassword, String newPassword, String newPasswordAgain) {
        if (!StringUtils.isNotBlank(oldPassword)){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),"参数错误");
        }
        String password = maintainerMapper.selectPassword(maintainerId);
        if(!password.equals(oldPassword)){
            return ServerResponse.createByErrorMassage("原密码错误");
        }
        if(!newPassword.equals(newPasswordAgain)){
            return ServerResponse.createByErrorMassage("新密码两次输入不一致");
        }
        if(newPassword.length() < 4){
            return ServerResponse.createByErrorMassage("密码长度不能小于4");
        }
        Maintainer maintainer = maintainerMapper.selectByPrimaryKey(Long.parseLong(maintainerId));

        maintainer.setMaintainerPassword(newPassword);

        int resultCount = maintainerMapper.updateByPrimaryKeySelective(maintainer);

        if(resultCount > 0){
            return ServerResponse.createBySuccess("修改成功");
        }
        return ServerResponse.createByErrorMassage("修改失败");
    }

    @Override
    public ServerResponse<PageInfo> selectComment(Integer pageNum, Integer pageSize, String maintainerId) {
        if (!StringUtils.isNotBlank(maintainerId)){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),"参数错误");
        }
        PageHelper.startPage(pageNum,pageSize);
        List<Comment> commentList = commentMapper.selectComment(maintainerId);
        if(commentList.size() == 0 || commentList == null){
            return ServerResponse.createByErrorMassage("还没有任何评价");
        }
        List<CommentVo> commentVoList = Lists.newArrayList();
        for (Comment comment:commentList) {
            Fault fault = faultMapper.selectByPrimaryKey(comment.getFaultId());
            CommentVo commentVo = createCommentVo(comment,fault);
            commentVoList.add(commentVo);
        }
        PageInfo pageInfo = new PageInfo(commentList);
        pageInfo.setList(commentVoList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    private CommentVo createCommentVo(Comment comment, Fault fault) {
        CommentVo commentVO = new CommentVo();
        commentVO.setCommentId(comment.getCommentId());
        commentVO.setCommentText(comment.getCommentText());
        commentVO.setCommentDate(comment.getCommentDate());
        commentVO.setFaultId(fault.getFaultId());
        commentVO.setFaultDescribe(fault.getFaultDescribe());
        commentVO.setFaultSite(fault.getFaultSite());
        return commentVO;
    }

    private RepairFaultVo createRepairFaultVo(Fault fault, List<FaultCategory> faultCategoryList) {
        RepairFaultVo repairFaultVo = new RepairFaultVo();
        repairFaultVo.setFaultCategoryId(fault.getFaultCategoryId());
        repairFaultVo.setFaultDescribe(fault.getFaultDescribe());
        repairFaultVo.setFaultId(fault.getFaultId());
        repairFaultVo.setFaultSite(fault.getFaultSite());
        repairFaultVo.setFaultWay(fault.getFaultWay());
        if(fault.getFaultWay() == 0){
            repairFaultVo.setFaultWayName("报修");
        }else if(fault.getFaultWay() == 1){
            repairFaultVo.setFaultWayName("回收");
        }
        for (FaultCategory faultCategory:faultCategoryList) {
            if(fault.getFaultCategoryId() == faultCategory.getFaultCategoryId()){
                repairFaultVo.setFaultCategoryName(faultCategory.getFaultCategoryName());
            }
        }
        return repairFaultVo;
    }

}

