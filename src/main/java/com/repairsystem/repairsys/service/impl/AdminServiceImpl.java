package com.repairsystem.repairsys.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.repairsystem.repairsys.common.Const;
import com.repairsystem.repairsys.common.ResponseCode;
import com.repairsystem.repairsys.common.ServerResponse;
import com.repairsystem.repairsys.dao.AdminMapper;
import com.repairsystem.repairsys.dao.FaultCategoryMapper;
import com.repairsystem.repairsys.dao.FaultMapper;
import com.repairsystem.repairsys.pojo.Admin;
import com.repairsystem.repairsys.pojo.Fault;
import com.repairsystem.repairsys.pojo.FaultCategory;
import com.repairsystem.repairsys.service.IAdminService;
import com.repairsystem.repairsys.vo.RepairFaultVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service("iAdminService")
@Transactional
public class AdminServiceImpl implements IAdminService{
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private FaultMapper faultMapper;
    @Autowired
    private FaultCategoryMapper faultCategoryMapper;

    @Override
    public ServerResponse<Admin> login(String adminNum, String adminPassword) {
        ServerResponse<String> validResponse = this.checkValid(adminNum, Const.NUM);
        if(validResponse.isSuccess()){
            return ServerResponse.createByErrorMassage("用户名不存在！！！");
        }
        Admin admin = adminMapper.selectLogin(adminNum, adminPassword);
        if(admin == null){
            return ServerResponse.createByErrorMassage("密码错误！！！");
        }
        admin.setAdminPassword(StringUtils.EMPTY);
        return ServerResponse.createBySuccess("登录成功！",admin);
    }

    public ServerResponse<String> checkValid(String str,String type){
        if(StringUtils.isNotBlank(type)){
            if(Const.NUM.equals(type)){
                int resultCount = adminMapper.checkAdminnum(str);
                if(resultCount > 0){
                    return ServerResponse.createByErrorMassage("用户名已存在");
                }
            }
            if(Const.TEL.equals(type)){
                int resultCount = adminMapper.checkTel(str);
                if(resultCount > 0){
                    return ServerResponse.createByErrorMassage("电话已存在");
                }
            }
        }else{
            return ServerResponse.createByErrorMassage("参数错误");
        }
        return ServerResponse.createBySuccessMassage("校验成功");
    }

    @Override
    public ServerResponse<String> addTentativeFaultOder(String faultId) {
        if(faultId == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),"参数错误");
        }
        Fault fault = faultMapper.selectByPrimaryKey(Long.parseLong(faultId));
        if(fault == null || fault.getFaultStatus() != 0){
            return ServerResponse.createByErrorMassage("订单号异常");
        }
        byte status = 1;
        fault.setFaultStatus(status);
        fault.setFaultTime(new Date());
        int resultCount = faultMapper.updateByPrimaryKeySelective(fault);
        if(resultCount > 0){
            return ServerResponse.createBySuccessMassage("派单成功");
        }
        return ServerResponse.createByErrorMassage("派单失败");
    }

    @Override
    public ServerResponse<String> addFaultCategory(FaultCategory faultCategory) {
        if(faultCategory == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),"参数错误");
        }
        int resultCount = faultCategoryMapper.insertSelective(faultCategory);
        if(resultCount > 0){
            return ServerResponse.createBySuccessMassage("添加成功");
        }
        return ServerResponse.createByErrorMassage("添加失败");
    }

    @Override
    public ServerResponse<String> deleteFaultCategory(String faultCategoryId) {
        if(faultCategoryId == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),"参数错误");
        }
        int resultC = faultMapper.checkFaultWithCategory(faultCategoryId);
        if(resultC > 0){
            return ServerResponse.createByErrorMassage("该类型正在被使用，不可删除");
        }
        int resultCount = faultCategoryMapper.deleteByPrimaryKey(Integer.parseInt(faultCategoryId));
        if(resultCount > 0){
            return ServerResponse.createBySuccessMassage("删除成功");
        }
        return ServerResponse.createByErrorMassage("删除失败");
    }

    @Override
    public ServerResponse<List> selectAllAdmin() {
        List<Admin> adminList = adminMapper.selectAllAdmin();

        if(adminList.size() == 0 || adminList == null){
            return ServerResponse.createByErrorMassage("没有任何员工信息");
        }else{
            return ServerResponse.createBySuccess(adminList);
        }
    }

    @Override
    public ServerResponse<PageInfo> selectAllUserOrder(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Fault> faultList = faultMapper.selectAllUserOrder();
        List<FaultCategory> faultCategoryList = faultCategoryMapper.selectAllFaultCategory();
        List<RepairFaultVo> repairFaultVoList = Lists.newArrayList();
        if(faultList == null || faultList.size() == 0){
            return ServerResponse.createByErrorMassage("暂时还没有用户订单！");
        }
        for (Fault fault:faultList) {
            RepairFaultVo repairFaultVo = createRepairFaultVo(fault,faultCategoryList);
            repairFaultVoList.add(repairFaultVo);
        }
        PageInfo pageInfo = new PageInfo(faultList);
        pageInfo.setList(repairFaultVoList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse<PageInfo> selectAllFaultCategoryByPage(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<FaultCategory> faultCategoryList = faultCategoryMapper.selectAllFaultCategory();
        PageInfo pageInfo = new PageInfo(faultCategoryList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse newPassword(String adminId, String oldPassword, String newPassword, String newPasswordAgain) {
        if (!StringUtils.isNotBlank(oldPassword)){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),"参数错误");
        }
        String password = adminMapper.selectPassword(adminId);
        if(!password.equals(oldPassword)){
            return ServerResponse.createByErrorMassage("原密码错误");
        }
        if(!newPassword.equals(newPasswordAgain)){
            return ServerResponse.createByErrorMassage("新密码两次输入不一致");
        }
        if(newPassword.length() < 4){
            return ServerResponse.createByErrorMassage("密码长度不能小于4");
        }
        Admin admin = adminMapper.selectByPrimaryKey(Long.parseLong(adminId));

        admin.setAdminPassword(newPassword);

        int resultCount = adminMapper.updateByPrimaryKeySelective(admin);

        if(resultCount > 0){
            return ServerResponse.createBySuccess("修改成功");
        }
        return ServerResponse.createByErrorMassage("修改失败");
    }

    @Override
    public ServerResponse deleteFault(String faultId) {
        if (!StringUtils.isNotBlank(faultId)){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),"参数错误");
        }
        int resultCount = faultMapper.checkFaultStatus(faultId,"1");
        if(resultCount < 0){
            return ServerResponse.createByErrorMassage("订单号异常");
        }
        Fault fault = faultMapper.selectByPrimaryKey(Long.parseLong(faultId));
        String userId = fault.getFaultUserId().toString();
        int deleteByPrimaryKey = faultMapper.deleteByPrimaryKey(Long.parseLong(faultId));
        if(deleteByPrimaryKey > 0){
            return ServerResponse.createBySuccess(userId);
        }
        return ServerResponse.createByErrorMassage("操作失败");
    }

    private RepairFaultVo createRepairFaultVo(Fault fault, List<FaultCategory> faultCategoryList) {
        RepairFaultVo repairFaultVo = new RepairFaultVo();
        repairFaultVo.setFaultCategoryId(fault.getFaultCategoryId());
        repairFaultVo.setFaultDescribe(fault.getFaultDescribe());
        repairFaultVo.setFaultId(fault.getFaultId());
        repairFaultVo.setFaultSite(fault.getFaultSite());
        repairFaultVo.setFaultWay(fault.getFaultWay());
        if(fault.getFaultWay() == 0){
            repairFaultVo.setFaultWayName("报修");
        }else if(fault.getFaultWay() == 1){
            repairFaultVo.setFaultWayName("回收");
        }
        for (FaultCategory faultCategory:faultCategoryList) {
            if(fault.getFaultCategoryId() == faultCategory.getFaultCategoryId()){
                repairFaultVo.setFaultCategoryName(faultCategory.getFaultCategoryName());
            }
        }
        return repairFaultVo;
    }

}
