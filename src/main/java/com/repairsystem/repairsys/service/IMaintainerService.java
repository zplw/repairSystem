package com.repairsystem.repairsys.service;

import com.github.pagehelper.PageInfo;
import com.repairsystem.repairsys.common.ServerResponse;
import com.repairsystem.repairsys.pojo.Maintainer;

import java.util.List;

public interface IMaintainerService {
    ServerResponse<Maintainer> login(String maintainerNum, String maintainerPassword);

    ServerResponse<String> checkVaild(String str,String type);

    ServerResponse<List> selectAllMaintain();

    ServerResponse<PageInfo> selectAllAdminOrder(Integer pageNum, Integer pageSize);

    ServerResponse takeOrder(String faultId, String maintainerId);

    ServerResponse selectMaintainerOrder(String maintainerId);

    ServerResponse successOrder(String faultId, String faultSolution);

    ServerResponse newPassword(String maintainerId, String oldPassword, String newPassword, String newPasswordAgain);

    ServerResponse<PageInfo> selectComment(Integer pageNum, Integer pageSize, String maintainerId);
}
