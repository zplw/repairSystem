package com.repairsystem.repairsys.service;

import com.github.pagehelper.PageInfo;
import com.repairsystem.repairsys.common.ServerResponse;
import com.repairsystem.repairsys.pojo.Fault;
import com.repairsystem.repairsys.pojo.User;

public interface IUserService {
    ServerResponse<User> login(String userNum, String userPassword);

    ServerResponse<String> checkValid(String str,String type);

    ServerResponse<PageInfo> list(Integer pageNum, Integer pageSize);

    ServerResponse<PageInfo> selectFaultByCategory(Integer pageNum, Integer pageSize, String faultCategoryId);

    ServerResponse selectAllFaultCategory();

    ServerResponse userPlaceOrder(Fault fault);

    ServerResponse<PageInfo> selectFaultByUserId(Integer pageNum, Integer pageSize, String userId);

    ServerResponse newPassword(String userId,String oldPassword, String newPassword, String newPasswordAgain);

    ServerResponse userComment(String commentText, String faultId, String maintainerId);
}
