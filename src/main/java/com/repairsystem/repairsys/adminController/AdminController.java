package com.repairsystem.repairsys.adminController;

import com.repairsystem.repairsys.common.Const;
import com.repairsystem.repairsys.common.ResponseCode;
import com.repairsystem.repairsys.common.ServerResponse;
import com.repairsystem.repairsys.pojo.Admin;
import com.repairsystem.repairsys.pojo.FaultCategory;
import com.repairsystem.repairsys.service.IAdminService;
import com.repairsystem.repairsys.service.IMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/admin/")
@CrossOrigin
public class AdminController {

    @Autowired
    private IAdminService iAdminService;
    @Autowired
    private IMailService iMailService;

    /**
     * 管理员登录
     * @param adminNum   管理员账号
     * @param adminPassword  管理员密码
     * @param session
     * @return
     */
    @RequestMapping(value = "login.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<Admin> login (String adminNum, String adminPassword, HttpSession session){
        ServerResponse<Admin> response = iAdminService.login(adminNum, adminPassword);
        if (response.isSuccess()){
            session.setAttribute(Const.CURRENT_ADMIN,response.getData());
        }
        return response;
    }

    /**
     * 管理员登出
     * @param session
     * @return
     */
    @RequestMapping(value = "logout.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> logout(HttpSession session){
        session.removeAttribute(Const.CURRENT_ADMIN);
        return ServerResponse.createBySuccess();
    }

    /**
     * 获取管理员登录信息
     * @param session
     * @return
     */
    @RequestMapping(value = "get_admin_info.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<Admin> getUserInfo(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin != null){
            return ServerResponse.createBySuccess(admin);
        }
        return ServerResponse.createByErrorMassage("用户未登录，无法获取当前用户信息");
    }


    /**
     * 添加故障类型
     * @param session
     * @param faultCategory
     * @return
     */
    @RequestMapping(value = "add_fault_category.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<String> addFaultCategory(HttpSession session, FaultCategory faultCategory){
        Admin admin = (Admin)session.getAttribute(Const.CURRENT_ADMIN);

        if(admin == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iAdminService.addFaultCategory(faultCategory);
    }

    /**
     * 删除故障类型
     * @param session
     * @param faultCategoryId
     * @return
     */
    @RequestMapping(value = "delete_fault_category.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<String> deleteFaultCategory(HttpSession session, String faultCategoryId){
        Admin admin = (Admin)session.getAttribute(Const.CURRENT_ADMIN);

        if(admin == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iAdminService.deleteFaultCategory(faultCategoryId);
    }

    /**
     * 查询所有管理员指定的信息
     * @return
     */
    @RequestMapping(value = "select_all_admin.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<List> selectAllAdmin(){
        return iAdminService.selectAllAdmin();
    }
    /**
     * 分页,查询所有待接受故障订单
     * @return
     */
    @RequestMapping(value = "select_all_user_order.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse selectAllUserOrder(@RequestParam(value = "pageNum",required = false,defaultValue = "1")Integer pageNum,
                                                   @RequestParam(value = "pageSize",required = false,defaultValue = "5")Integer pageSize,
                                                   HttpSession session){
        Admin admin = (Admin)session.getAttribute(Const.CURRENT_ADMIN);

        if(admin == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iAdminService.selectAllUserOrder(pageNum,pageSize);
    }

    /**
     * 分页，检索所有故障类型
     * @param session
     * @return
     */
    @RequestMapping(value = "select_all_fault_category_by_page.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse selectAllFaultCategoryByPage(@RequestParam(value = "pageNum",required = false,defaultValue = "1")Integer pageNum,
                                                       @RequestParam(value = "pageSize",required = false,defaultValue = "5")Integer pageSize,
                                                       HttpSession session){
        Admin admin = (Admin)session.getAttribute(Const.CURRENT_ADMIN);

        if(admin == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iAdminService.selectAllFaultCategoryByPage(pageNum,pageSize);
    }

    /**
     * 更改密码
     * @param session
     * @param oldPassword
     * @param newPassword
     * @param newPasswordAgain
     * @return
     */
    @RequestMapping(value = "new_password.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse newPassword(HttpSession session,String adminId,String oldPassword,String newPassword,String newPasswordAgain) {
        Admin admin = (Admin)session.getAttribute(Const.CURRENT_ADMIN);

        if(admin == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iAdminService.newPassword(adminId,oldPassword,newPassword,newPasswordAgain);
    }

    /**
     * 发送邮件提醒
     */
    @RequestMapping(value = "send_email.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse sendEmail(HttpSession session,String userId,String cause) {
        Admin admin = (Admin)session.getAttribute(Const.CURRENT_ADMIN);

        if(admin == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iMailService.sendEmail(userId,cause);
    }

    /**
     * 删除被拒绝的订单
     */
    @RequestMapping(value = "delete_fault.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse deleteFault(HttpSession session,String faultId) {
        Admin admin = (Admin)session.getAttribute(Const.CURRENT_ADMIN);

        if(admin == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iAdminService.deleteFault(faultId);
    }
}
