package com.repairsystem.repairsys.common;

import com.google.common.collect.Sets;

import java.util.Set;

public class Const {
    public static final String EMPTY="";

    public static final String CURRENT_USER = "currentUser";

    public static final String CURRENT_ADMIN = "currentAdmin";

    public static final String CURRENT_MAINTAINER = "currentMaintainer";

    public static final String TEL = "tel";

    public static final String NUM = "Num";

    public static final String FTP_FOLDER_NAME ="img";

    //使用一个接口类充当一个用户组
    public interface Role{
        Byte ROLE_CUSTOMER = 0;
        Byte ROLE_ADMIN = 1;
    }

    public interface Cart{
        int CHECKED = 1;//选中状态
        int UN_CHECKED = 0;//未选中状态

        String LIMIT_NUM_FAIL = "LIMIT_NUM_FAIL";
        String LIMIT_NUM_SUCCESS = "LIMIT_NUM_SUCCESS";
    }

    public interface ProductListOrderBy{
        Set<String> PRICE_ASC_DESC =Sets.newHashSet("price_desc","price_asc");
    }

    public enum ProductStatusEnum {

        ON_SALE(1,"在线");


        private int code;
        private String value;

        ProductStatusEnum(int code,String value){
            this.code = code;
            this.value = value;
        }

        public int getCode() {
            return code;
        }

        public String getValue() {
            return value;
        }
    }
}
