package com.repairsystem.repairsys.pojo;

public class Admin {
    private Long adminId;

    private String adminNum;

    private String adminPassword;

    private String adminName;

    private String adminTel;

    private String adminImg;

    public Admin(String adminName, String adminImg) {
        this.adminName = adminName;
        this.adminImg = adminImg;
    }

    public Admin(Long adminId, String adminNum, String adminPassword, String adminName, String adminTel, String adminImg) {
        this.adminId = adminId;
        this.adminNum = adminNum;
        this.adminPassword = adminPassword;
        this.adminName = adminName;
        this.adminTel = adminTel;
        this.adminImg = adminImg;
    }

    public Admin() {
        super();
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getAdminNum() {
        return adminNum;
    }

    public void setAdminNum(String adminNum) {
        this.adminNum = adminNum == null ? null : adminNum.trim();
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword == null ? null : adminPassword.trim();
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName == null ? null : adminName.trim();
    }

    public String getAdminTel() {
        return adminTel;
    }

    public void setAdminTel(String adminTel) {
        this.adminTel = adminTel == null ? null : adminTel.trim();
    }

    public String getAdminImg() {
        return adminImg;
    }

    public void setAdminImg(String adminImg) {
        this.adminImg = adminImg == null ? null : adminImg.trim();
    }
}