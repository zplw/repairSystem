package com.repairsystem.repairsys.pojo;

import java.util.Date;

public class Fault {
    private Long faultId;

    private String faultDescribe;

    private Integer faultCategoryId;

    private String faultSite;

    private String faultSolution;

    private Byte faultWay;

    private Date faultTime;

    private Date faultSolutionTime;

    private Long faultMaintainerId;

    private Long faultUserId;

    private Byte faultStatus;

    public Fault(Long faultId, String faultDescribe, Integer faultCategoryId, String faultSite, String faultSolution, Byte faultWay, Date faultTime, Date faultSolutionTime, Long faultMaintainerId, Long faultUserId, Byte faultStatus) {
        this.faultId = faultId;
        this.faultDescribe = faultDescribe;
        this.faultCategoryId = faultCategoryId;
        this.faultSite = faultSite;
        this.faultSolution = faultSolution;
        this.faultWay = faultWay;
        this.faultTime = faultTime;
        this.faultSolutionTime = faultSolutionTime;
        this.faultMaintainerId = faultMaintainerId;
        this.faultUserId = faultUserId;
        this.faultStatus = faultStatus;
    }

    public Fault() {
        super();
    }

    public Long getFaultId() {
        return faultId;
    }

    public void setFaultId(Long faultId) {
        this.faultId = faultId;
    }

    public String getFaultDescribe() {
        return faultDescribe;
    }

    public void setFaultDescribe(String faultDescribe) {
        this.faultDescribe = faultDescribe == null ? null : faultDescribe.trim();
    }

    public Integer getFaultCategoryId() {
        return faultCategoryId;
    }

    public void setFaultCategoryId(Integer faultCategoryId) {
        this.faultCategoryId = faultCategoryId;
    }

    public String getFaultSite() {
        return faultSite;
    }

    public void setFaultSite(String faultSite) {
        this.faultSite = faultSite == null ? null : faultSite.trim();
    }

    public String getFaultSolution() {
        return faultSolution;
    }

    public void setFaultSolution(String faultSolution) {
        this.faultSolution = faultSolution == null ? null : faultSolution.trim();
    }

    public Byte getFaultWay() {
        return faultWay;
    }

    public void setFaultWay(Byte faultWay) {
        this.faultWay = faultWay;
    }

    public Date getFaultTime() {
        return faultTime;
    }

    public void setFaultTime(Date faultTime) {
        this.faultTime = faultTime;
    }

    public Date getFaultSolutionTime() {
        return faultSolutionTime;
    }

    public void setFaultSolutionTime(Date faultSolutionTime) {
        this.faultSolutionTime = faultSolutionTime;
    }

    public Long getFaultMaintainerId() {
        return faultMaintainerId;
    }

    public void setFaultMaintainerId(Long faultMaintainerId) {
        this.faultMaintainerId = faultMaintainerId;
    }

    public Long getFaultUserId() {
        return faultUserId;
    }

    public void setFaultUserId(Long faultUserId) {
        this.faultUserId = faultUserId;
    }

    public Byte getFaultStatus() {
        return faultStatus;
    }

    public void setFaultStatus(Byte faultStatus) {
        this.faultStatus = faultStatus;
    }
}