package com.repairsystem.repairsys.pojo;

import java.util.Date;

public class Comment {
    private Long commentId;

    private String commentText;

    private Long faultId;

    private Date commentDate;

    private Long maintainerId;

    public Comment(Long commentId, String commentText, Long faultId, Date commentDate, Long maintainerId) {
        this.commentId = commentId;
        this.commentText = commentText;
        this.faultId = faultId;
        this.commentDate = commentDate;
        this.maintainerId = maintainerId;
    }

    public Comment() {
        super();
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText == null ? null : commentText.trim();
    }

    public Long getFaultId() {
        return faultId;
    }

    public void setFaultId(Long faultId) {
        this.faultId = faultId;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public Long getMaintainerId() {
        return maintainerId;
    }

    public void setMaintainerId(Long maintainerId) {
        this.maintainerId = maintainerId;
    }
}