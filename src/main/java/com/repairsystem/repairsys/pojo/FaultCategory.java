package com.repairsystem.repairsys.pojo;

public class FaultCategory {
    private Integer faultCategoryId;

    private String faultCategoryName;

    public FaultCategory(Integer faultCategoryId, String faultCategoryName) {
        this.faultCategoryId = faultCategoryId;
        this.faultCategoryName = faultCategoryName;
    }

    public FaultCategory() {
        super();
    }

    public Integer getFaultCategoryId() {
        return faultCategoryId;
    }

    public void setFaultCategoryId(Integer faultCategoryId) {
        this.faultCategoryId = faultCategoryId;
    }

    public String getFaultCategoryName() {
        return faultCategoryName;
    }

    public void setFaultCategoryName(String faultCategoryName) {
        this.faultCategoryName = faultCategoryName == null ? null : faultCategoryName.trim();
    }
}