package com.repairsystem.repairsys.pojo;

public class Maintainer {
    private Long maintainerId;

    private String maintainerNum;

    private String maintainerPassword;

    private String maintainerName;

    private Integer maintainerTotal;

    private String maintainerTel;

    private Byte maintainerStatus;

    private String maintainerImg;

    public Maintainer(String maintainerName, String maintainerImg) {
        this.maintainerName = maintainerName;
        this.maintainerImg = maintainerImg;
    }

    public Maintainer( Long maintainerId,String maintainerName) {
        this.maintainerId = maintainerId;
        this.maintainerName = maintainerName;
    }

    public Maintainer(Long maintainerId, String maintainerNum, String maintainerPassword, String maintainerName, Integer maintainerTotal, String maintainerTel, Byte maintainerStatus, String maintainerImg) {
        this.maintainerId = maintainerId;
        this.maintainerNum = maintainerNum;
        this.maintainerPassword = maintainerPassword;
        this.maintainerName = maintainerName;
        this.maintainerTotal = maintainerTotal;
        this.maintainerTel = maintainerTel;
        this.maintainerStatus = maintainerStatus;
        this.maintainerImg = maintainerImg;
    }

    public Maintainer() {
        super();
    }

    public Long getMaintainerId() {
        return maintainerId;
    }

    public void setMaintainerId(Long maintainerId) {
        this.maintainerId = maintainerId;
    }

    public String getMaintainerNum() {
        return maintainerNum;
    }

    public void setMaintainerNum(String maintainerNum) {
        this.maintainerNum = maintainerNum == null ? null : maintainerNum.trim();
    }

    public String getMaintainerPassword() {
        return maintainerPassword;
    }

    public void setMaintainerPassword(String maintainerPassword) {
        this.maintainerPassword = maintainerPassword == null ? null : maintainerPassword.trim();
    }

    public String getMaintainerName() {
        return maintainerName;
    }

    public void setMaintainerName(String maintainerName) {
        this.maintainerName = maintainerName == null ? null : maintainerName.trim();
    }

    public Integer getMaintainerTotal() {
        return maintainerTotal;
    }

    public void setMaintainerTotal(Integer maintainerTotal) {
        this.maintainerTotal = maintainerTotal;
    }

    public String getMaintainerTel() {
        return maintainerTel;
    }

    public void setMaintainerTel(String maintainerTel) {
        this.maintainerTel = maintainerTel == null ? null : maintainerTel.trim();
    }

    public Byte getMaintainerStatus() {
        return maintainerStatus;
    }

    public void setMaintainerStatus(Byte maintainerStatus) {
        this.maintainerStatus = maintainerStatus;
    }

    public String getMaintainerImg() {
        return maintainerImg;
    }

    public void setMaintainerImg(String maintainerImg) {
        this.maintainerImg = maintainerImg == null ? null : maintainerImg.trim();
    }
}