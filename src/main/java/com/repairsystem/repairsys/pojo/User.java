package com.repairsystem.repairsys.pojo;

import java.util.Date;

public class User {
    private Long userId;

    private String userNum;

    private String userPassword;

    private String userName;

    private Date userBirth;

    private String userTel;

    private String userEmail;

    public User(Long userId, String userNum, String userPassword, String userName, Date userBirth, String userTel, String userEmail) {
        this.userId = userId;
        this.userNum = userNum;
        this.userPassword = userPassword;
        this.userName = userName;
        this.userBirth = userBirth;
        this.userTel = userTel;
        this.userEmail = userEmail;
    }

    public User() {
        super();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserNum() {
        return userNum;
    }

    public void setUserNum(String userNum) {
        this.userNum = userNum == null ? null : userNum.trim();
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword == null ? null : userPassword.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Date getUserBirth() {
        return userBirth;
    }

    public void setUserBirth(Date userBirth) {
        this.userBirth = userBirth;
    }

    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel == null ? null : userTel.trim();
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail == null ? null : userEmail.trim();
    }
}