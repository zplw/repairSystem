package com.repairsystem.repairsys.maintainerController;


import com.repairsystem.repairsys.common.Const;
import com.repairsystem.repairsys.common.ResponseCode;
import com.repairsystem.repairsys.common.ServerResponse;
import com.repairsystem.repairsys.pojo.Maintainer;
import com.repairsystem.repairsys.service.IMaintainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/maintainer/")
@CrossOrigin
public class MaintainerController {
    @Autowired
    private IMaintainerService iMaintainerService;

    /**
     * 维修员登录
     * @param maintainerNum   维修员工账号
     * @param maintainerPassword  维修员工密码
     * @param session
     * @return
     */
    @RequestMapping(value = "login.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<Maintainer> login (String maintainerNum, String maintainerPassword, HttpSession session){
        ServerResponse<Maintainer> response = iMaintainerService.login(maintainerNum, maintainerPassword);
        if (response.isSuccess()){
            session.setAttribute(Const.CURRENT_MAINTAINER,response.getData());
        }
        return response;
    }

    /**
     * 维修员登出
     * @param session
     * @return
     */
    @RequestMapping(value = "logout.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> logout(HttpSession session){
        session.removeAttribute(Const.CURRENT_MAINTAINER);
        return ServerResponse.createBySuccess();
    }

    /**
     * 获取维修员登录信息
     * @param session
     * @return
     */
    @RequestMapping(value = "get_maintainer_info.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<Maintainer> getUserInfo(HttpSession session){
        Maintainer maintainer = (Maintainer) session.getAttribute(Const.CURRENT_MAINTAINER);
        if(maintainer != null){
            return ServerResponse.createBySuccess(maintainer);
        }
        return ServerResponse.createByErrorMassage("用户未登录，无法获取当前用户信息");
    }

    /**
     * 查询所有维修员指定的信息
     * @return
     */
    @RequestMapping(value = "select_all_maintainer.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<List> selectAllMaintain(HttpSession session){
        return iMaintainerService.selectAllMaintain();
    }

    /**
     * 分页,查询所有已接受待接单的订单信息
     * @return
     */
    @RequestMapping(value = "select_all_admin_order.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse selectAllAdminOrder(@RequestParam(value = "pageNum",required = false,defaultValue = "1")Integer pageNum,
                                                    @RequestParam(value = "pageSize",required = false,defaultValue = "5")Integer pageSize,
                                                    HttpSession session){
        Maintainer maintainer = (Maintainer) session.getAttribute(Const.CURRENT_MAINTAINER);

        if(maintainer == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iMaintainerService.selectAllAdminOrder(pageNum,pageSize);
    }

    /**
     * 查询所有已接单的订单信息
     * @return
     */
    @RequestMapping(value = "select_maintainer_order.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse selectMaintainerOrder(HttpSession session,String maintainerId){
        Maintainer maintainer = (Maintainer) session.getAttribute(Const.CURRENT_MAINTAINER);

        if(maintainer == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iMaintainerService.selectMaintainerOrder(maintainerId);
    }


    /**
     * 完成订单
     * @return
     */
    @RequestMapping(value = "success_order.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse successOrder(HttpSession session,String faultId,String faultSolution){
        Maintainer maintainer = (Maintainer) session.getAttribute(Const.CURRENT_MAINTAINER);

        if(maintainer == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iMaintainerService.successOrder(faultId,faultSolution);
    }

    /**
     * 更改密码
     * @param session
     * @param oldPassword
     * @param newPassword
     * @param newPasswordAgain
     * @return
     */
    @RequestMapping(value = "new_password.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse newPassword(HttpSession session,String maintainerId,String oldPassword,String newPassword,String newPasswordAgain) {
        Maintainer maintainer = (Maintainer) session.getAttribute(Const.CURRENT_MAINTAINER);

        if(maintainer == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iMaintainerService.newPassword(maintainerId,oldPassword,newPassword,newPasswordAgain);
    }


    /**
     * 分页，查看评价
     * @return
     */
    @RequestMapping(value = "select_comment.do",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse selectComment(@RequestParam(value = "pageNum",required = false,defaultValue = "1")Integer pageNum,
                                        @RequestParam(value = "pageSize",required = false,defaultValue = "5")Integer pageSize,
                                        HttpSession session,String maintainerId){
        Maintainer maintainer = (Maintainer) session.getAttribute(Const.CURRENT_MAINTAINER);

        if(maintainer == null){
            return ServerResponse.createByErrorCodeMassage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录");
        }
        return iMaintainerService.selectComment(pageNum,pageSize,maintainerId);
    }
}
