package com.repairsystem.repairsys.dao;

import com.repairsystem.repairsys.pojo.Admin;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminMapper {
    int deleteByPrimaryKey(Long adminId);

    int insert(Admin record);

    int insertSelective(Admin record);

    Admin selectByPrimaryKey(Long adminId);

    int updateByPrimaryKeySelective(Admin record);

    int updateByPrimaryKey(Admin record);

    Admin selectLogin(@Param("adminNum") String adminNum,@Param("adminPassword") String adminPassword);

    int checkAdminnum(String adminNum);

    int checkTel(String tel);

    List<Admin> selectAllAdmin();

    String selectPassword(String adminId);
}