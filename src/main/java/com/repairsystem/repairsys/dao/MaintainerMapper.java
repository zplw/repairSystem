package com.repairsystem.repairsys.dao;

import com.repairsystem.repairsys.pojo.Comment;
import com.repairsystem.repairsys.pojo.Maintainer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MaintainerMapper {
    int deleteByPrimaryKey(Long maintainerId);

    int insert(Maintainer record);

    int insertSelective(Maintainer record);

    Maintainer selectByPrimaryKey(Long maintainerId);

    int updateByPrimaryKeySelective(Maintainer record);

    int updateByPrimaryKey(Maintainer record);

    Maintainer selectLogin(@Param("maintainerNum") String maintainerNum, @Param("maintainerPassword") String maintainerPassword);

    int checkMaintainnum(String maintainNum);

    int checkTel(String tel);

    List<Maintainer> selectAllMaintain();

    List<Maintainer> selectAllMaintainIdAndName();

    String selectPassword(String maintainerId);
}