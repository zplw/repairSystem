package com.repairsystem.repairsys.dao;

import com.repairsystem.repairsys.pojo.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    int deleteByPrimaryKey(Long userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int checkUsernum(String userNum);

    int checkTel(String tel);

    User selectLogin(@Param("userNum")String userNum, @Param("userPassword") String userPassword);

    String selectPassword(String userId);

}