package com.repairsystem.repairsys.dao;

import com.repairsystem.repairsys.pojo.Fault;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FaultMapper {
    int deleteByPrimaryKey(Long faultId);

    int insert(Fault record);

    int insertSelective(Fault record);

    Fault selectByPrimaryKey(Long faultId);

    int updateByPrimaryKeySelective(Fault record);

    int updateByPrimaryKey(Fault record);

    List<Fault> selectAllFault();

    List<Fault> selectByFaultCategoryId(long faultCategoryId);

    List<Fault> selectAllUserOrder();

    List<Fault> selectAllAdminOrder();

    int selectMaintainerTakeOrderById(String maintainerId);

    int checkFaultStatus(@Param("faultId")String faultId,@Param("faultStatus")String faultStatus);

    Fault selectMaintainerOrder(@Param("maintainerId") String maintainerId);

    int checkFaultWithCategory(String faultCategoryId);

    List<Fault> selectFaultByUserId(String userId);

    int checkFaultWithMaintainerId(@Param("faultId")String faultId,@Param("maintainerId") String maintainerId);
}