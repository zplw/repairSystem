package com.repairsystem.repairsys.dao;

import com.repairsystem.repairsys.pojo.FaultCategory;

import java.util.List;

public interface FaultCategoryMapper {
    int deleteByPrimaryKey(Integer faultCategoryId);

    int insert(FaultCategory record);

    int insertSelective(FaultCategory record);

    FaultCategory selectByPrimaryKey(Integer faultCategoryId);

    int updateByPrimaryKeySelective(FaultCategory record);

    int updateByPrimaryKey(FaultCategory record);

    List<FaultCategory> selectAllFaultCategory();
}