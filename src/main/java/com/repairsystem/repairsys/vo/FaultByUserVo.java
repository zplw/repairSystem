package com.repairsystem.repairsys.vo;

public class FaultByUserVo {
    private Long faultId;

    private Integer faultCategoryId;

    private String faultCategoryName;

    private String faultDescribe;

    private String faultSite;

    private Byte faultStatus;

    private String faultStatusName;

    private Long faultMaintainerId;

    public Long getFaultMaintainerId() {
        return faultMaintainerId;
    }

    public void setFaultMaintainerId(Long faultMaintainerId) {
        this.faultMaintainerId = faultMaintainerId;
    }

    public Long getFaultId() {
        return faultId;
    }

    public void setFaultId(Long faultId) {
        this.faultId = faultId;
    }

    public Integer getFaultCategoryId() {
        return faultCategoryId;
    }

    public void setFaultCategoryId(Integer faultCategoryId) {
        this.faultCategoryId = faultCategoryId;
    }

    public String getFaultCategoryName() {
        return faultCategoryName;
    }

    public void setFaultCategoryName(String faultCategoryName) {
        this.faultCategoryName = faultCategoryName;
    }

    public String getFaultDescribe() {
        return faultDescribe;
    }

    public void setFaultDescribe(String faultDescribe) {
        this.faultDescribe = faultDescribe;
    }

    public String getFaultSite() {
        return faultSite;
    }

    public void setFaultSite(String faultSite) {
        this.faultSite = faultSite;
    }

    public Byte getFaultStatus() {
        return faultStatus;
    }

    public void setFaultStatus(Byte faultStatus) {
        this.faultStatus = faultStatus;
    }

    public String getFaultStatusName() {
        return faultStatusName;
    }

    public void setFaultStatusName(String faultStatusName) {
        this.faultStatusName = faultStatusName;
    }
}
