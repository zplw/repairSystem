package com.repairsystem.repairsys.vo;

public class RepairFaultVo {
    private Long faultId;

    private String faultDescribe;

    private Integer faultCategoryId;

    private String faultSite;

    private Byte faultWay;

    private String faultWayName;

    private String faultCategoryName;

    public Long getFaultId() {
        return faultId;
    }

    public void setFaultId(Long faultId) {
        this.faultId = faultId;
    }

    public String getFaultDescribe() {
        return faultDescribe;
    }

    public void setFaultDescribe(String faultDescribe) {
        this.faultDescribe = faultDescribe;
    }

    public Integer getFaultCategoryId() {
        return faultCategoryId;
    }

    public void setFaultCategoryId(Integer faultCategoryId) {
        this.faultCategoryId = faultCategoryId;
    }

    public String getFaultSite() {
        return faultSite;
    }

    public void setFaultSite(String faultSite) {
        this.faultSite = faultSite;
    }

    public Byte getFaultWay() {
        return faultWay;
    }

    public void setFaultWay(Byte faultWay) {
        this.faultWay = faultWay;
    }

    public String getFaultWayName() {
        return faultWayName;
    }

    public void setFaultWayName(String faultWayName) {
        this.faultWayName = faultWayName;
    }

    public String getFaultCategoryName() {
        return faultCategoryName;
    }

    public void setFaultCategoryName(String faultCategoryName) {
        this.faultCategoryName = faultCategoryName;
    }
}
