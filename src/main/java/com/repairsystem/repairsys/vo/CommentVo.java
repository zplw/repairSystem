package com.repairsystem.repairsys.vo;

import java.util.Date;

public class CommentVo {
    private Long commentId;

    private String commentText;

    private Long faultId;

    private Date commentDate;

    private String faultDescribe;

    private String faultSite;

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Long getFaultId() {
        return faultId;
    }

    public void setFaultId(Long faultId) {
        this.faultId = faultId;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public String getFaultDescribe() {
        return faultDescribe;
    }

    public void setFaultDescribe(String faultDescribe) {
        this.faultDescribe = faultDescribe;
    }

    public String getFaultSite() {
        return faultSite;
    }

    public void setFaultSite(String faultSite) {
        this.faultSite = faultSite;
    }
}
