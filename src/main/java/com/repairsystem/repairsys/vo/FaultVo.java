package com.repairsystem.repairsys.vo;

import java.util.Date;

public class FaultVo {

    private Long faultId;

    private String faultDescribe;

    private Integer faultCategoryId;

    private String faultSite;

    private String faultSolution;

    private Byte faultWay;

    private Date faultTime;

    private Date faultSolutionTime;

    private Long faultMaintainerId;

    private Byte faultStatus;

    private String faultCategoryName;

    private String maintainerName;

    public Long getFaultId() {
        return faultId;
    }

    public void setFaultId(Long faultId) {
        this.faultId = faultId;
    }

    public String getFaultDescribe() {
        return faultDescribe;
    }

    public void setFaultDescribe(String faultDescribe) {
        this.faultDescribe = faultDescribe;
    }

    public Integer getFaultCategoryId() {
        return faultCategoryId;
    }

    public void setFaultCategoryId(Integer faultCategoryId) {
        this.faultCategoryId = faultCategoryId;
    }

    public String getFaultSite() {
        return faultSite;
    }

    public void setFaultSite(String faultSite) {
        this.faultSite = faultSite;
    }

    public String getFaultSolution() {
        return faultSolution;
    }

    public void setFaultSolution(String faultSolution) {
        this.faultSolution = faultSolution;
    }

    public Byte getFaultWay() {
        return faultWay;
    }

    public void setFaultWay(Byte faultWay) {
        this.faultWay = faultWay;
    }

    public Date getFaultTime() {
        return faultTime;
    }

    public void setFaultTime(Date faultTime) {
        this.faultTime = faultTime;
    }

    public Date getFaultSolutionTime() {
        return faultSolutionTime;
    }

    public void setFaultSolutionTime(Date faultSolutionTime) {
        this.faultSolutionTime = faultSolutionTime;
    }

    public Long getFaultMaintainerId() {
        return faultMaintainerId;
    }

    public void setFaultMaintainerId(Long faultMaintainerId) {
        this.faultMaintainerId = faultMaintainerId;
    }

    public Byte getFaultStatus() {
        return faultStatus;
    }

    public void setFaultStatus(Byte faultStatus) {
        this.faultStatus = faultStatus;
    }

    public String getFaultCategoryName() {
        return faultCategoryName;
    }

    public void setFaultCategoryName(String faultCategoryName) {
        this.faultCategoryName = faultCategoryName;
    }

    public String getMaintainerName() {
        return maintainerName;
    }

    public void setMaintainerName(String maintainerName) {
        this.maintainerName = maintainerName;
    }

}
